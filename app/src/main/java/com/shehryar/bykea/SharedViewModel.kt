package com.shehryar.bykea

import androidx.lifecycle.MutableLiveData
import com.shehryar.bykea.baseclasses.BaseViewModel


/**
 * Shared View Model class for sharing data between fragments
 */
class SharedViewModel : BaseViewModel() {

    val clickOnContinueBtn : MutableLiveData<Boolean> ?= null

}