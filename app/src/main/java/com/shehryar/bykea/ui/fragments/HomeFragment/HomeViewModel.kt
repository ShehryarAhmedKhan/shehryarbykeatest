package com.shehryar.bykea.ui.fragments.HomeFragment


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.shehryar.bykea.baseclasses.BaseViewModel
import com.shehryar.bykea.data.models.MusicResponseModel
import com.shehryar.bykea.data.remote.Resource
import com.shehryar.bykea.data.remote.reporitory.MainRepository
import com.shehryar.bykea.utils.NetworkHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class HomeViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper
) : BaseViewModel() {

    private val _getMusics = MutableLiveData<Resource<MusicResponseModel>>()
    val getMusics: LiveData<Resource<MusicResponseModel>>
        get() = _getMusics

    fun hitGetMusicAPI() {
        viewModelScope.launch {
            _getMusics.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                try {
                    mainRepository.getMusics().let {
                        if (it.isSuccessful) {
                            _getMusics.postValue(Resource.success(it.body()!!))
                        } else if (it.code() == 500 || it.code() == 404 || it.code() == 400) {
                            _getMusics.postValue(Resource.error(it.message(), null))
                        } else {
                            _getMusics.postValue(Resource.error("Some thing went wrong", null))
                        }
                    }
                } catch (e: Exception) {
                    _getMusics.postValue(Resource.error("${e.message}", null))
                }
            } else _getMusics.postValue(Resource.error("No internet connection", null))
        }
    }

}