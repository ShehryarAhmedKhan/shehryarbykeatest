package com.shehryar.bykea.ui.fragments.temp

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import com.shehryar.bykea.R
import com.shehryar.bykea.baseclasses.BaseFragment
import com.shehryar.bykea.databinding.ActivitySplashBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TempFragment : BaseFragment<ActivitySplashBinding, TempViewModel>() {

    override val layoutId: Int
        get() = R.layout.activity_splash
    override val viewModel: Class<TempViewModel>
        get() = TempViewModel::class.java
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickListener()
    }

    private fun clickListener() {

    }

}