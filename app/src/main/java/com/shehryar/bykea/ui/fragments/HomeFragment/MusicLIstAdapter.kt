package com.shehryar.bykea.ui.fragments.HomeFragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.shehryar.bykea.R
import com.shehryar.bykea.data.models.MusicModel
import com.shehryar.bykea.databinding.ItemMusicListBinding
import com.squareup.picasso.Picasso

class MusicLIstAdapter(val onItemClickListener: OnItemClickListener) : ListAdapter<MusicModel, MusicLIstAdapter.ViewHolder>(DiffCallBack()) {

    var lastSelectedPostion = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicLIstAdapter.ViewHolder {
        return ViewHolder(ItemMusicListBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: MusicLIstAdapter.ViewHolder, position: Int) {
        holder.bind.musicModel = getItem(position)
        if(getItem(position).artworkUrl100 != null){
            Picasso.get().load(getItem(position).artworkUrl100).into(holder.bind.thumbNail)
        }else{
            Picasso.get().load(R.drawable.logo).into(holder.bind.thumbNail)
        }
        if(getItem(position).isPlaying){
            holder.bind.playStatus.visibility = View.VISIBLE
        }else{
            holder.bind.playStatus.visibility = View.GONE
        }
        holder.itemView.setOnClickListener {
            if(lastSelectedPostion != -1){
                getItem(lastSelectedPostion).isPlaying = false
            }
            lastSelectedPostion = position
            getItem(position).isPlaying = true
            onItemClickListener.onItemClick(position, getItem(position))
        }
    }

    class ViewHolder(private var binding: ItemMusicListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val bind = binding
    }

    class DiffCallBack : DiffUtil.ItemCallback<MusicModel>() {

        override fun areItemsTheSame(oldItem: MusicModel, newItem: MusicModel): Boolean {
            return oldItem.artistId == newItem.artistId
        }

        override fun areContentsTheSame(oldItem: MusicModel, newItem: MusicModel): Boolean {
            return oldItem.artistId == newItem.artistId && oldItem.isPlaying == newItem.isPlaying
        }

    }

    interface OnItemClickListener {
        fun onItemClick(itemPosition: Int, musicModel: MusicModel)
    }

}