package com.shehryar.bykea.ui.activity.mainActivity

import com.shehryar.bykea.baseclasses.BaseViewModel
import com.shehryar.bykea.data.remote.reporitory.MainRepository
import com.shehryar.bykea.utils.NetworkHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper
) : BaseViewModel() {


}
