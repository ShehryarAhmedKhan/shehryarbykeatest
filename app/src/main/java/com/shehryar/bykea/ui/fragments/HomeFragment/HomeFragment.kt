package com.shehryar.bykea.ui.fragments.HomeFragment

import android.media.MediaPlayer
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.shehryar.bykea.R
import com.shehryar.bykea.baseclasses.BaseFragment
import com.shehryar.bykea.data.models.MusicModel
import com.shehryar.bykea.data.remote.Resource
import com.shehryar.bykea.databinding.FragmentHomeBinding
import com.shehryar.bykea.utils.DialogHelperClass
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {


    override val layoutId: Int
        get() = R.layout.fragment_home
    override val viewModel: Class<HomeViewModel>
        get() = HomeViewModel::class.java
    override val bindingVariable: Int
        get() = BR.viewModel

    lateinit var musicListAdapter: MusicLIstAdapter
    lateinit var bottomSheetMusicControl: BottomSheetBehavior<RelativeLayout>
    var mMediaPlayer: MediaPlayer ?= null
    private var mIsMediaPlayerPrepared = false
    var selectedMusicModel: MusicModel ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.hitGetMusicAPI()
        initAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        mViewDataBinding.musicList.adapter = musicListAdapter
        bottomSheetMusicControl = BottomSheetBehavior.from<RelativeLayout>(mViewDataBinding.musicPlayerBottomSheet.musicPlayerController)
        mMediaPlayer?.let {
            if(it.isPlaying){
                mViewDataBinding.musicPlayerBottomSheet.playBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_baseline_pause_circle_outline_24))
            }else{
                mViewDataBinding.musicPlayerBottomSheet.playBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_baseline_play_arrow_24))
            }
        }
    }

    private fun initAdapter() {
        musicListAdapter = MusicLIstAdapter(object : MusicLIstAdapter.OnItemClickListener {
            override fun onItemClick(itemPosition: Int, musicModel: MusicModel) {
                if(selectedMusicModel != null){
                    if(selectedMusicModel!!.trackId != null && selectedMusicModel!!.trackId.equals(musicModel.trackId)){
                        return
                    }
                }

                selectedMusicModel = musicModel
                if(!TextUtils.isEmpty(musicModel.previewUrl)){
                    preparePlayer(musicModel.previewUrl)
                }else{
                    Toast.makeText(requireContext(),"Some thing wrong",Toast.LENGTH_SHORT).show()
                    bottomSheetMusicControl.setState(BottomSheetBehavior.STATE_COLLAPSED)
                    return
                }

                musicListAdapter.notifyDataSetChanged()

                bottomSheetMusicControl.setState(BottomSheetBehavior.STATE_EXPANDED);

                if(musicModel.artworkUrl100 != null){
                    Picasso.get().load(musicModel.artworkUrl100).into(mViewDataBinding.musicPlayerBottomSheet.thumbNail)
                }else{
                    Picasso.get().load(R.drawable.logo).into(mViewDataBinding.musicPlayerBottomSheet.thumbNail)
                }

                mViewDataBinding.musicPlayerBottomSheet.tvMusicName.setText(musicModel.artistName)
                mViewDataBinding.musicPlayerBottomSheet.tvArtistName.setText(musicModel.collectionName)

                mViewDataBinding.musicPlayerBottomSheet.playBtn.setOnClickListener {
                    if(mIsMediaPlayerPrepared){
                        mMediaPlayer?.let {
                            if(it.isPlaying){
                                mViewDataBinding.musicPlayerBottomSheet.playBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_baseline_play_arrow_24))
                                it.pause()
                            }else{
                                mViewDataBinding.musicPlayerBottomSheet.playBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_baseline_pause_circle_outline_24))
                                it.start()
                            }
                        }
                    }
                }
            }
        })
    }

    override fun subscribeToNetworkLiveData() {
        super.subscribeToNetworkLiveData()

        mViewModel.getMusics.observe(this, Observer {
            when (it.status) {
                Resource.Status.LOADING -> {
                    loadingDialog.show()
                }
                Resource.Status.SUCCESS -> {
                    loadingDialog.dismiss()
                    it.data?.let {
                        it.results?.let {
                            if(it.isNotEmpty() && this::musicListAdapter.isInitialized){
                                musicListAdapter.submitList(it)
                            }
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    loadingDialog.dismiss()
                    DialogHelperClass.errorDialog(requireContext(), it.message!!)
                }
            }
        })

    }

    private fun preparePlayer(playLink: String) {
        mIsMediaPlayerPrepared = false
            mViewDataBinding.musicPlayerBottomSheet.playBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_baseline_play_arrow_24))
            mViewDataBinding.musicPlayerBottomSheet.playBtn.visibility = View.GONE
            mViewDataBinding.musicPlayerBottomSheet.spinKit.visibility = View.VISIBLE
        if (mMediaPlayer != null) {
            releaseMediaPlayer()
        }

        mMediaPlayer = MediaPlayer()
        mMediaPlayer!!.setDataSource(playLink)
        mMediaPlayer!!.prepareAsync()

        mMediaPlayer?.setOnPreparedListener {
//            it.start()
            mIsMediaPlayerPrepared = true
            mViewDataBinding.musicPlayerBottomSheet.spinKit.visibility = View.GONE
            mViewDataBinding.musicPlayerBottomSheet.playBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_baseline_play_arrow_24))
            mViewDataBinding.musicPlayerBottomSheet.playBtn.visibility = View.VISIBLE
        }

        mMediaPlayer?.setOnCompletionListener {
            mViewDataBinding.musicPlayerBottomSheet.playBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_baseline_play_arrow_24))
        }


    }


    fun releaseMediaPlayer() {
        mMediaPlayer?.let {
            it.stop()
            it.reset()
            it.release()
            mMediaPlayer = null
        }

    }


}