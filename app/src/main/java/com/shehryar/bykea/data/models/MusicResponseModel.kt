package com.shehryar.bykea.data.models

data class MusicResponseModel(
    val resultCount: Int,
    val results: List<MusicModel>?,
)