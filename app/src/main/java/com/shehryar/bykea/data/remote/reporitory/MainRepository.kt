package com.shehryar.bykea.data.remote.reporitory

import com.shehryar.bykea.data.local.db.AppDao
import com.shehryar.bykea.data.remote.ApiService
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiService: ApiService,
    localDataSource: AppDao
) {

    suspend fun getMusics() = apiService.getMusics()

}