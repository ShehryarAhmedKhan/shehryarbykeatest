package com.shehryar.bykea.data.remote


import com.shehryar.bykea.data.models.MusicModel
import com.shehryar.bykea.data.models.MusicResponseModel
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("lookup?amgArtistId=1055684&entity=song")
    suspend fun getMusics(): Response<MusicResponseModel>
}