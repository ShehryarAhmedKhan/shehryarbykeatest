package com.shehryar.bykea.baseclasses

import androidx.lifecycle.ViewModel


open class BaseViewModel : ViewModel() {
}